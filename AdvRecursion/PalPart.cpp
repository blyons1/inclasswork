/**********************************************
* File: PalPart.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the solution and main driver
* for find the partitions of a string which are
* a palindrome 
**********************************************/

#include <iostream>		// cout, endl 
#include <vector>

/********************************************
* Function Name  : isPalindrome
* Pre-conditions : std::string strPal
* Post-conditions: bool
* 
* Determines if the string is a palindrome or not 
********************************************/
bool isPalindrome(std::string strPal){
	
	if(strPal.length() == 0){
		return true;
	}
	
	for(int i = 0; i < strPal.length(); i++){
		if(strPal[i] != strPal[strPal.length() - i - 1])
			return false;
	}
	
	return true;
}


/********************************************
* Function Name  : testPalindrome
* Pre-conditions : std::string strPal
* Post-conditions: none
* 
* A function to test all cases of isPalindrome 
********************************************/
void testPalindrome(std::string strPal){
	
	std::cout << "The string '" << strPal << "' is ";
	
	if(!isPalindrome(strPal))
		std::cout << "not ";
	
	std::cout << "a palindrome." << std::endl;
}

/********************************************
* Function Name  : isPalinPart
* Pre-conditions : std::string strPal, int low, int high
* Post-conditions: bool
* 
* Calculates if a substring of the string is a
* Palindrom from low to high 
********************************************/
bool isPalinPart(std::string strPal, int low, int high){
	
	if(strPal.length() == 0)
		return true;

    while (low < high) 
    { 
        if (strPal[low] != strPal[high]) 
            return false; 
		
        low++; high--; 
    } 
    return true; 	
	
}

/********************************************
* Function Name  : testPalinPart
* Pre-conditions : std::string strPal, int low, int high
* Post-conditions: none
* 
* Tests a case and a substring (low to high) if that
* partition is a Palindrome 
********************************************/
void testPalinPart(std::string strPal, int low, int high){
	
	std::cout << "String is '" << strPal << "', low = " << low << ", high = " << high;
	std::cout << ". The test results in bool = " << isPalinPart(strPal, low, high);
	std::cout << std::endl;
}

/********************************************
* Function Name  : testPalRun
* Pre-conditions : none
* Post-conditions: none
*
* Runs the test cases. Called from main  
********************************************/
void testPalRun(){
	testPalindrome("");
	
	testPalindrome("a");
	
	testPalindrome("abc");
	
	testPalindrome("abcba");
	
	testPalindrome("abccba");
}

/********************************************
* Function Name  : testPalPartRun
* Pre-conditions : none
* Post-conditions: none
* 
* Tests a set of test cases for the Palindrom
* Partition for one case only 
********************************************/
void testPalPartRun(){
	testPalinPart("", 0, 0);
	
	testPalinPart("a", 0, 0);
	
	testPalinPart("abcba", 1, 3);

	testPalinPart("abcba", 0, 4);

	testPalinPart("abcba", 1, 4);

	testPalinPart("abcba", 0, 3);
}

/********************************************
* Function Name  : allPalPartUtil
* Pre-conditions : std::vector< std::vector< std::string > >& allPart, 
				   std::vector< std::string >& currPart, 
				   std::string str, size_t start, size_t n
* Post-conditions: none
*  
********************************************/
void allPalPartUtil(std::vector< std::vector< std::string > >& allPart, 
					std::vector< std::string >& currPart, 
					std::string str, size_t start, size_t n) 
{ 
    // If 'start' has reached len 
    if (start >= n) 
    { 
        allPart.push_back(currPart); 
        return; 
    } 
  
    // Pick all possible ending points for substrings 
    for (int i=start; i<n; i++) 
    { 
		// std::cout << start << "\t" << i << "\t" << i-start+1 << "\t\t" << str.substr(start, i-start+1) << "\t\t\t\t";
        // If substring str[start..i] is palindrome 
        if (isPalinPart(str, start, i)) 
        { 
			//std::cout << " is a Palindrome!" << std::endl;
	
            // Add the substring to result 
            currPart.push_back(str.substr(start, i-start+1)); 
  
            // Recur for remaining remaining substring 
            allPalPartUtil(allPart, currPart, str, i+1, n); 
              
            // Remove substring str[start..i] from current  
            // partition 
            currPart.pop_back(); 
        } 
		/*else{
			std::cout << std::endl;
		}*/
    } 
} 
  
/********************************************
* Function Name  : allPalPartitions
* Pre-conditions : std::string str
* Post-conditions: none
* 
* Gets all the Palindromic Partitions of the
* input string 
********************************************/
void allPalPartitions(std::string str) 
{ 

	std::cout << "Input string = '" << str << "'" << std::endl;
	std::cout << "Palindromic Partitions: " << std::endl;
	// std::cout << "start\ti\t(i-start+1)\tstr.substr(start, i-start+1)\tIsPartitionPalindrome?" <<  std::endl;
    size_t n = str.length(); 
  
    // To Store all palindromic partitions 
    std::vector<std::vector<std::string> > allPartitions; 
  
    // To store current palindromic partition 
    std::vector<std::string> currPartition;  
  
    // Call recursive function to generate all partiions 
    // and store in allPartitions 
    allPalPartUtil(allPartitions, currPartition, str, 0, n); 
  
    // Print all partitions generated by above call 
	std::cout << "Total Partition Combinations = " << allPartitions.size() << std::endl;
    for (size_t i=0; i < allPartitions.size(); i++ ) 
    { 
        for (size_t j=0; j<allPartitions[i].size(); j++) 
            std::cout << allPartitions[i][j] << " "; 
        std::cout << "\n"; 
    } 
	
	std::cout << "------------" << std::endl;
} 

/********************************************
* Function Name  : allPalPartRun
* Pre-conditions : none
* Post-conditions: none
* 
* Tests all the cases for the Palindrom Partition 
********************************************/
void testPalinPartRun(){
	
    allPalPartitions(""); 
	
    allPalPartitions("a"); 
	
    allPalPartitions("abc");

    allPalPartitions("abcdcbefgfeh");

    allPalPartitions("abcdcbbcdcba");
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(int argc, char** argv){

	//testPalRun();  		// Successfully tested
	//testPalPartRun();	// Successfully tested
	
	testPalinPartRun();
	
	return 0;
}    



